# Production
yarn add body-parser
yarn add cookie-parser
yarn add cors
yarn add csurf
yarn add dotenv
yarn add errorhandler
yarn add express
yarn add jsonwebtoken
yarn add method-override
yarn add morgan
yarn add nodemailer
yarn add smtp-server

# Develop
yarn add -D typescript
yarn add -D tslint
yarn add -D @types/body-parser
yarn add -D @types/cookie-parser
yarn add -D @types/cors
yarn add -D @types/csurf
yarn add -D @types/dotenv
yarn add -D @types/errorhandler
yarn add -D @types/express
yarn add -D @types/jsonwebtoken
yarn add -D @types/method-override
yarn add -D @types/morgan
yarn add -D @types/node
yarn add -D @types/nodemailer
yarn add -D @types/smtp-server

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changePasswordCheck = exports.resetPasswordCheck = exports.loginCheck = exports.signUpCheck = void 0;
const user_model_1 = __importDefault(require("./user.model"));
const bcrypt_1 = __importDefault(require("bcrypt"));
function signUpCheck({ email }) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield user_model_1.default.findOne({ where: { email } }).catch((error) => {
            throw new Error("Database error - User.findOne");
        });
        const isAlreadyExistUser = user;
        if (isAlreadyExistUser)
            return false;
        return true;
    });
}
exports.signUpCheck = signUpCheck;
function loginCheck({ email, password }) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield user_model_1.default.findOne({ where: { email } }).catch((error) => {
            console.log(error);
            throw new Error("Database error - User.findOne");
        });
        const isNotFoundUser = !user;
        if (isNotFoundUser)
            return false;
        console.log("Find User!");
        const isNotCorrectPassword = !(yield bcrypt_1.default
            .compare(password, user.password || "")
            .catch((error) => {
            throw new Error("Bcrypt error");
        }));
        if (isNotCorrectPassword)
            return false;
        console.log("Correnct Password!");
        const isNotConfirmed = !user.is_confirmed;
        if (isNotConfirmed)
            return false;
        console.log("User Confirmed!");
        return true;
    });
}
exports.loginCheck = loginCheck;
function resetPasswordCheck({ email }) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield user_model_1.default.findOne({
            where: { email }
        }).catch((error) => {
            console.log(error);
            throw new Error("Database error - User.findOne by reset_password_token");
        });
        const isNotFoundUser = !user;
        if (isNotFoundUser)
            return false;
        return true;
    });
}
exports.resetPasswordCheck = resetPasswordCheck;
function changePasswordCheck({ password, newPassword, reset_password_token }) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield user_model_1.default.findOne({
            where: { reset_password_token }
        }).catch((error) => {
            console.log(error);
            throw new Error("Database error - User.findOne by reset_password_token");
        });
        const isNotFoundUser = !user;
        if (isNotFoundUser)
            return false;
        return true;
    });
}
exports.changePasswordCheck = changePasswordCheck;

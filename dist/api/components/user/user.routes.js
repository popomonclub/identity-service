"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_controller_1 = __importDefault(require("./user.controller"));
const router = express_1.default.Router();
router.post("/signUp", user_controller_1.default.signUpRequest);
router.get("/confirmEmail/:email_confirm_token", user_controller_1.default.confirmEmailRequest);
router.post("/login", user_controller_1.default.loginRequest);
router.post("/resetPassword", user_controller_1.default.resetPasswordRequest);
router.put("/changePassword", user_controller_1.default.changePasswordRequest);
exports.default = router;

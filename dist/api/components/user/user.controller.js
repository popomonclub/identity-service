"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const userValidation = __importStar(require("./user.validation"));
const userService = __importStar(require("./user.service"));
const generateToken_1 = __importDefault(require("../../helpers/generateToken"));
const user_model_1 = __importDefault(require("./user.model"));
const sendEmail_1 = __importDefault(require("../../helpers/sendEmail"));
const env_1 = require("../../env");
const matched_1 = __importDefault(require("../../helpers/matched"));
const signUpValidation = [
    userValidation.email,
    userValidation.password,
    userValidation.passwordConfirmation,
    userValidation.phone
];
function signUp(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        // console.log("POST - signUp 요청이 도착했습니다.");
        // Validation - Regex
        // console.log("POST - signUp :: Validation - Regex");
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        // RequestParameter + RequestBody
        // console.log("POST - signUp :: RequestParameter + RequestBody");
        const { email, password, passwordConfirmation, phone } = (0, matched_1.default)(req);
        // Validation - Process
        // console.log("POST - signUp :: Validation - Process");
        const isValid = yield userService.signUpCheck({ email }).catch((error) => {
            console.log("회원가입 체크 에러!!!");
            res.status(500).json({ code: 500 });
        });
        if (isValid) {
            const token = (0, generateToken_1.default)(email);
            user_model_1.default.create({ email, password, email_confirm_token: token })
                .then((user) => {
                (0, sendEmail_1.default)({
                    type: "CONFIRM_EMAIL",
                    to: email,
                    token,
                    redirectUrl: "",
                    success: () => {
                        res.status(200).json({ code: 200 });
                    },
                    fail: () => {
                        res.status(500).json({ code: 500 });
                    }
                });
            })
                .catch((error) => {
                console.log(error);
            });
        }
        else {
            res.status(409).json({ code: 409 });
        }
    });
}
const confirmEmailValidation = [];
function confirmEmail(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { email_confirm_token } = (0, matched_1.default)(req);
        user_model_1.default.update({
            is_confirmed: true,
            email_confirm_token: null
        }, {
            where: {
                email_confirm_token
            }
        })
            .then((data) => {
            const [number, _] = data;
            if (number > 0) {
                res.status(200).json({ code: 200 });
            }
            else {
                res.status(400).json({ code: 400 });
            }
        })
            .catch((error) => {
            res.status(500).json({ code: 500 });
        });
    });
}
const loginValidation = [];
function login(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ code: 400 });
        }
        const { email, password } = (0, matched_1.default)(req);
        const isValid = yield userService
            .loginCheck({ email, password })
            .catch((error) => {
            console.log(error);
            res.status(500).json({ code: 500 });
        });
        if (isValid) {
            const accessToken = (0, generateToken_1.default)({ username: req.body.username });
            res.cookie(env_1.ACCESS_TOKEN_NAME, accessToken, {
                httpOnly: true
            });
            res.status(200).json({ code: 200, token: accessToken });
        }
        else {
            res.status(401).json({ code: 401 });
        }
    });
}
const resetPasswordValidation = [];
function resetPassword(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { email, redirect_url } = (0, matched_1.default)(req);
        const isValid = yield userService.resetPasswordCheck({ email }).catch((error) => {
            console.log(error);
            res.status(500).json({ code: 500 });
        });
        ;
        if (isValid) {
            const token = (0, generateToken_1.default)(email);
            user_model_1.default.update({ reset_password_token: token }, { where: { email } })
                .then((user) => {
                (0, sendEmail_1.default)({
                    type: "RESET_PASSWORD",
                    to: email,
                    token,
                    redirectUrl: `${redirect_url}`,
                    success: () => {
                        res.status(200).json({ code: 200, token });
                    },
                    fail: () => {
                        res.status(500).json({ code: 500 });
                    }
                });
            })
                .catch((error) => {
                res.status(500).json({ code: 500 });
            });
        }
        else {
            res.status(400).json({ code: 400 });
        }
    });
}
const changePasswordValidation = [
    userValidation.password,
    userValidation.newPassword
];
function changePassword(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { password, newPassword, reset_password_token } = (0, matched_1.default)(req);
        const isValid = yield userService.changePasswordCheck({
            password,
            newPassword,
            reset_password_token
        });
        if (isValid) {
            user_model_1.default.update({ password, reset_password_token: null }, { where: { reset_password_token } })
                .then((user) => {
                res.status(200).json({ code: 200 });
            })
                .catch((error) => {
                res.status(500).json({ code: 500 });
            });
        }
        else {
            res.status(400).json({ code: 400 });
        }
        res.json({ code: 200 });
    });
}
exports.default = {
    loginRequest: [...loginValidation, login],
    signUpRequest: [...signUpValidation, signUp],
    confirmEmailRequest: [...confirmEmailValidation, confirmEmail],
    resetPasswordRequest: [...resetPasswordValidation, resetPassword],
    changePasswordRequest: [...changePasswordValidation, changePassword]
};

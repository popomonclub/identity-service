"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var csrf = require("csurf");
exports.default = csrf({ cookie: true });

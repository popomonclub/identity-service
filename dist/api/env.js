"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GMAIL_PASSWORD = exports.GMAIL_USERNAME = exports.DB_NAME = exports.DB_HOST = exports.DB_PASSWORD = exports.DB_USERNAME = exports.ACCESS_TOKEN_SECRET = exports.ACCESS_TOKEN_NAME = void 0;
// Access token
exports.ACCESS_TOKEN_NAME = process.env.ACCESS_TOKEN_NAME || "";
exports.ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET || "";
// Database
exports.DB_USERNAME = process.env.DB_USERNAME || "";
exports.DB_PASSWORD = process.env.DB_PASSWORD || "";
exports.DB_HOST = process.env.DB_HOST || "";
exports.DB_NAME = process.env.DB_NAME || "";
// Google - gmail
exports.GMAIL_USERNAME = process.env.GMAIL_USERNAME || "";
exports.GMAIL_PASSWORD = process.env.GMAIL_PASSWORD || "";

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
function connectDatabase() {
    const DB_USERNAME = process.env.DB_USERNAME || "";
    const DB_PASSWORD = process.env.DB_PASSWORD || "";
    const DB_HOST = process.env.DB_HOST || "";
    const DB_NAME = process.env.DB_NAME || "";
    const sequelize = new sequelize_typescript_1.Sequelize({
        username: DB_USERNAME,
        password: DB_PASSWORD,
        database: DB_NAME,
        host: DB_HOST,
        dialect: "mysql",
        models: [__dirname + "/**/*.model.js", __dirname + "/**/*.model.ts"],
        modelMatch: (filename, member) => {
            return (filename.substring(0, filename.indexOf(".model")) ===
                member.toLowerCase());
        }
    });
    return sequelize;
}
exports.default = connectDatabase;

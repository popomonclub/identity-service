"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const cors_1 = __importDefault(require("cors"));
const errorhandler_1 = __importDefault(require("errorhandler"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const user_routes_1 = __importDefault(require("../api/components/user/user.routes"));
const csurf_1 = __importDefault(require("csurf"));
// import { router } from "./api";
const app = (0, express_1.default)();
const isDevelopment = process.env.NODE_ENV === "development";
// CORS - widthCredentials
app.use((0, cors_1.default)({
    origin: true,
    credentials: true
}));
// Cookie
app.use((0, cookie_parser_1.default)(process.env.COOKIE_SECRET));
// Log
app.use(require("morgan")("dev"));
// application/json
// application/x-www-form-urlencoded
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
// CSRF Token
app.use((0, csurf_1.default)({
    cookie: true
}));
// When client request method is PUT or DELET in <form> tag or <a> tag
app.use(require("method-override")());
if (isDevelopment) {
    app.use((0, errorhandler_1.default)());
}
app.get("/csrfToken", function (req, res) {
    res.cookie("XSRF-TOKEN", req.csrfToken(), {
        expires: new Date(Date.now() + 3 * 3600000) // 3시간 동안 유효
    });
    res.json({
        csrfToken: req.csrfToken(),
        position: 'req.headers["csrf-token"]'
    });
});
// User
app.use("/user", user_routes_1.default);
// Catch 403 - CSRF TOKEN
app.use(function (err, req, res, next) {
    if (err.code !== "EBADCSRFTOKEN")
        return next(err);
    res.status(403);
    next(err);
});
// Catch 404
app.use((req, res, next) => {
    const err = new Error("Not found");
    res.status(404); // using response here
    next(err);
});
// Catch 500
app.use((err, _req, res, _next) => {
    if (isDevelopment) {
        console.log(err);
    }
    res.status(err.status || 500).json({
        errors: Array.isArray(err.message) ? err.message : { message: err.message }
    });
});
exports.default = app;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function matched(req) {
    return Object.assign(Object.assign(Object.assign({}, req.query), req.body), req.params);
}
exports.default = matched;

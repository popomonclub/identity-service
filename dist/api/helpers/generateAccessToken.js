"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function generateAccessToken(username) {
  return jsonwebtoken_1.default.sign(username, "12312312", {
    expiresIn: "1800s" // 30 min
  });
}
exports.default = generateAccessToken;

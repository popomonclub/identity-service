"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = __importDefault(require("nodemailer"));
const nodemailer_smtp_transport_1 = __importDefault(require("nodemailer-smtp-transport"));
const env_1 = require("../env");
const serverUrl = process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : "http://identity.popomon.club";
const resetPasswordSubject = "[포포몬 클럽] 비밀번호 초기화";
const resetPasswordTemplate = (reset_password_token, redirect_url) => `
  <table>
    <tr>
      <a href="${redirect_url}?token=${reset_password_token}">
        비밀번호 새롭게 설정하기
      </a>
    </tr>
  </table>
`;
const confirmEmailSubject = "[포포몬 클럽] 본인인증 이메일";
const confirmEmailTemplate = (email_confirm_token, redirect_url) => `
  <table>
    <tr>
      <a href="${serverUrl}/confirmEmail/${email_confirm_token}?redirectUrl=${redirect_url}">
        본인의 이메일이 맞습니다
      </a>
    </tr>
  </table>
`;
function sendEmail({ to, type, token, redirectUrl, success, fail }) {
    const transporter = nodemailer_1.default.createTransport((0, nodemailer_smtp_transport_1.default)({
        service: "gmail",
        host: "smtp.gmail.com",
        auth: {
            user: env_1.GMAIL_USERNAME,
            pass: env_1.GMAIL_PASSWORD
        }
    }));
    transporter.sendMail({
        from: env_1.GMAIL_USERNAME,
        to: to || env_1.GMAIL_USERNAME,
        subject: type === "RESET_PASSWORD" ? resetPasswordSubject : confirmEmailSubject,
        html: type === "RESET_PASSWORD"
            ? resetPasswordTemplate(token, redirectUrl)
            : confirmEmailTemplate(token, redirectUrl)
    }, function (error, info) {
        if (error) {
            console.log(error);
            fail();
        }
        else {
            console.log("Email sent: " + info.response);
            success();
        }
    });
}
exports.default = sendEmail;

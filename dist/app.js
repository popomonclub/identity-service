"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = exports.server = void 0;
const http_1 = require("http");
const db_1 = __importDefault(require("./api/db"));
const server_1 = __importDefault(require("./api/server"));
console.log("MODE :: " + process.env.NODE_ENV);
const server = (0, http_1.createServer)(server_1.default);
exports.server = server;
const db = (0, db_1.default)();
exports.db = db;
server.listen(3000, () => {
    console.log(`Server listening on port ${3000}`);
});

import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import errorhandler from "errorhandler";
import dotenv from "dotenv";
dotenv.config();
import userRouter from "../api/components/user/user.routes";
import csurf from "csurf";

// import { router } from "./api";
const app = express();
const isDevelopment = process.env.NODE_ENV === "development";

// CORS - widthCredentials
app.use(
  cors({
    origin: true,
    credentials: true
  })
);

// Cookie
app.use(cookieParser(process.env.COOKIE_SECRET));

// Log
app.use(require("morgan")("dev"));

// application/json
// application/x-www-form-urlencoded
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CSRF Token
app.use(
  csurf({
    cookie: true
  })
);

// When client request method is PUT or DELET in <form> tag or <a> tag
app.use(require("method-override")());

if (isDevelopment) {
  app.use(errorhandler());
}

app.get("/csrfToken", function (req, res) {
  res.cookie("XSRF-TOKEN", req.csrfToken(), {
    expires: new Date(Date.now() + 3 * 3600000) // 3시간 동안 유효
  });

  res.json({
    csrfToken: req.csrfToken(),
    position: 'req.headers["csrf-token"]'
  });
});

// User
app.use("/user", userRouter);

// Catch 403 - CSRF TOKEN
app.use(function (err, req, res, next) {
  if (err.code !== "EBADCSRFTOKEN") return next(err);
  res.status(403);
  next(err);
});

// Catch 404
app.use((req, res, next) => {
  const err = new Error("Not found");
  res.status(404); // using response here
  next(err);
});

// Catch 500
app.use((err, _req, res, _next) => {
  if (isDevelopment) {
    console.log(err);
  }

  res.status(err.status || 500).json({
    errors: Array.isArray(err.message) ? err.message : { message: err.message }
  });
});

export default app;

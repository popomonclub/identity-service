// Access token
export const ACCESS_TOKEN_NAME = process.env.ACCESS_TOKEN_NAME || "";
export const ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET || "";

// Database
export const DB_USERNAME = process.env.DB_USERNAME || "";
export const DB_PASSWORD = process.env.DB_PASSWORD || "";
export const DB_HOST = process.env.DB_HOST || "";
export const DB_NAME = process.env.DB_NAME || "";

// Google - gmail
export const GMAIL_USERNAME = process.env.GMAIL_USERNAME || "";
export const GMAIL_PASSWORD = process.env.GMAIL_PASSWORD || "";

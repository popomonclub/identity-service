import {
  Table,
  Column,
  Model,
  PrimaryKey,
  CreatedAt,
  UpdatedAt,
  BeforeCreate,
  BeforeUpdate,
  HasMany
} from "sequelize-typescript";
import bcrypt from "bcrypt";
import Project from "../project/project.model";

@Table({ tableName: "manager" }) // Default: TableName + 's' (Users)
class Manager extends Model {
  @BeforeUpdate
  static updateLog(instance: Manager) {
    console.log("Update manager", instance.email);
  }

  @BeforeCreate
  static createLog(instance: Manager) {
    console.log("Create manager", instance.email);
    instance.is_confirmed = false;
    instance.password_updated_at = new Date();
    instance.password = bcrypt.hashSync(
      instance.password || "",
      bcrypt.genSaltSync(10)
    );
  }

  @PrimaryKey
  @Column
  readonly id!: number;

  @Column
  email?: string;

  @Column
  password?: string;
  passwordConfirmation?: string;
  newPassword?: string;

  @Column
  email_confirm_token?: string;

  @Column
  reset_password_token?: string;

  @Column
  is_confirmed?: boolean;

  @Column
  password_updated_at?: Date;

  @CreatedAt
  createdAt?: Date;

  @UpdatedAt
  updatedAt?: Date;

  @HasMany(() => Project)
  projects?: Project[]
}

export default Manager;

import { Request, Response } from "express";
import {
  ValidationChain,
  validationResult
} from "express-validator";
import {
  ConfirmEmailParams,
  LoginParams,
  ResetPasswordParams,
  SignUpParams,
  ChangePasswordParams
} from "./manager.type";
import * as managerValidation from "./manager.validation";
import * as managerService from "./manager.service";
import generateToken from "../../helpers/generateToken";
import Manager from "./manager.model";
import sendEmail from "../../helpers/sendEmail";
import { ACCESS_TOKEN_NAME } from "../../env";
import matched from "../../helpers/matched";

const signUpValidation: ValidationChain[] = [
  managerValidation.email,
  managerValidation.password,
  managerValidation.passwordConfirmation,
  managerValidation.phone
];

async function signUp(req: Request, res: Response) {
  // console.log("POST - signUp 요청이 도착했습니다.");

  // Validation - Regex
  // console.log("POST - signUp :: Validation - Regex");
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  // RequestParameter + RequestBody
  // console.log("POST - signUp :: RequestParameter + RequestBody");
  const { email, password, passwordConfirmation, phone } =
    matched(req) as SignUpParams;

  // Validation - Process
  // console.log("POST - signUp :: Validation - Process");
  const isValid = await managerService.signUpCheck({ email }).catch((error) => {
    console.log("회원가입 체크 에러!!!");
    res.status(500).json({ code: 500 });
  });

  if (isValid) {
    const token = generateToken(email);
    Manager.create({ email, password, email_confirm_token: token })
      .then((manager) => {
        sendEmail({
          type: "CONFIRM_EMAIL",
          to: email,
          token,
          redirectUrl: "",
          success: () => {
            res.status(200).json({ code: 200 });
          },
          fail: () => {
            res.status(500).json({ code: 500 });
          }
        });
      })
      .catch((error) => {
        console.log(error);
      });
  } else {
    res.status(409).json({ code: 409 });
  }
}

const confirmEmailValidation: ValidationChain[] = [];

async function confirmEmail(req: Request, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email_confirm_token } = matched(req) as ConfirmEmailParams;

  Manager.update(
    {
      is_confirmed: true,
      email_confirm_token: null
    },
    {
      where: {
        email_confirm_token
      }
    }
  )
    .then((data) => {
      const [number, _] = data;

      if (number > 0) {
        res.status(200).json({ code: 200 });
      } else {
        res.status(400).json({ code: 400 });
      }
    })
    .catch((error) => {
      res.status(500).json({ code: 500 });
    });
}

const loginValidation: ValidationChain[] = [];

async function login(req: Request, res: Response) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ code: 400 });
  }

  const { email, password } = matched(req) as LoginParams;

  const isValid = await managerService
    .loginCheck({ email, password })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ code: 500 });
    });

  if (isValid) {
    const accessToken = generateToken({ managername: req.body.managername });
    res.cookie(ACCESS_TOKEN_NAME, accessToken, {
      httpOnly: true
    });
    res.status(200).json({ code: 200, token: accessToken });
  } else {
    res.status(401).json({ code: 401 });
  }
}

const resetPasswordValidation: ValidationChain[] = [];

async function resetPassword(req: Request, res: Response) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email, redirect_url } = matched(req) as ResetPasswordParams;
  const isValid = await managerService.resetPasswordCheck({ email }).catch((error) => {
    console.log(error);
    res.status(500).json({ code: 500 });
  });;
  
  if (isValid) {
    const token = generateToken(email);
    Manager.update({ reset_password_token: token }, { where: { email } })
      .then((manager) => {
        sendEmail({
          type: "RESET_PASSWORD",
          to: email,
          token,
          redirectUrl: `${redirect_url}`,
          success: () => {
            res.status(200).json({ code: 200, token });
          },
          fail: () => {
            res.status(500).json({ code: 500 });
          }
        });
      })
      .catch((error) => {
        res.status(500).json({ code: 500 });
      });
  } else {
    res.status(400).json({ code: 400 });
  }
}

const changePasswordValidation: ValidationChain[] = [
  managerValidation.password,
  managerValidation.newPassword
];

async function changePassword(req: Request, res: Response) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { password, newPassword, reset_password_token } = matched(req) as ChangePasswordParams;
  const isValid = await managerService.changePasswordCheck({
    password,
    newPassword,
    reset_password_token
  });

  if(isValid){
    Manager.update({ password, reset_password_token: null }, { where: { reset_password_token } })
      .then((manager) => {
        res.status(200).json({ code: 200 });
      })
      .catch((error) => {
        res.status(500).json({ code: 500 });
      });
  } else {
    res.status(400).json({ code: 400 });
  }

  res.json({ code: 200 });
}

export default {
  loginRequest: [...loginValidation, login],
  signUpRequest: [...signUpValidation, signUp],
  confirmEmailRequest: [...confirmEmailValidation, confirmEmail],
  resetPasswordRequest: [...resetPasswordValidation, resetPassword],
  changePasswordRequest: [...changePasswordValidation, changePassword]
};

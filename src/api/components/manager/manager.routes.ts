import express from "express";
import managerController from "./manager.controller";

const router = express.Router();

router.post("/signUp", managerController.signUpRequest);
router.get(
  "/confirmEmail/:email_confirm_token",
  managerController.confirmEmailRequest
);
router.post("/login", managerController.loginRequest);
router.post("/resetPassword", managerController.resetPasswordRequest);
router.put("/changePassword", managerController.changePasswordRequest);

export default router;

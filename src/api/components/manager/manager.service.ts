import Manager from "./manager.model";
import bcrypt from "bcrypt";

export async function signUpCheck({ email }): Promise<boolean> {
  const manager = await Manager.findOne<Manager>({ where: { email } }).catch((error) => {
    throw new Error("Database error - Manager.findOne");
  });

  const isAlreadyExistManager = manager;
  if (isAlreadyExistManager) return false;

  return true;
}

export async function loginCheck({ email, password }): Promise<boolean> {
  const manager = await Manager.findOne<Manager>({ where: { email } }).catch((error) => {
    console.log(error);
    throw new Error("Database error - Manager.findOne");
  });

  const isNotFoundManager = !manager;
  if (isNotFoundManager) return false;
  console.log("Find Manager!");

  const isNotCorrectPassword = !(await bcrypt
    .compare(password, manager.password || "")
    .catch((error) => {
      throw new Error("Bcrypt error");
    }));
  if (isNotCorrectPassword) return false;
  console.log("Correnct Password!");

  const isNotConfirmed = !manager.is_confirmed;
  if (isNotConfirmed) return false;
  console.log("Manager Confirmed!");

  return true;
}

export async function resetPasswordCheck({
  email
}): Promise<boolean> {
  const manager = await Manager.findOne<Manager>({
    where: { email }
  }).catch((error) => {
    console.log(error);
    throw new Error("Database error - Manager.findOne by reset_password_token");
  });

  const isNotFoundManager = !manager;
  if (isNotFoundManager) return false;

  return true;
}

export async function changePasswordCheck({
  password,
  newPassword,
  reset_password_token
}): Promise<boolean> {
  const manager = await Manager.findOne<Manager>({
    where: { reset_password_token }
  }).catch((error) => {
    console.log(error);
    throw new Error("Database error - Manager.findOne by reset_password_token");
  });

  const isNotFoundManager = !manager;
  if (isNotFoundManager) return false;

  return true;
}

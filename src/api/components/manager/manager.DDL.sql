CREATE TABLE `manager` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '기본키',
  `email` varchar(320) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '이메일',
  `password` varchar(72) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '비밀번호',
  `email_confirm_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '본인확인 이메일 토큰 (회원가입 후 계정 활성화)',
  `reset_password_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '비밀번호 초기화 토큰 (비밀번호 초기화)',
  `is_confirmed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '본인확인 이메일 확인여부',
  `createdAt` datetime DEFAULT NULL COMMENT '생성일',
  `updatedAt` datetime DEFAULT NULL COMMENT '수정일',
  `password_updated_at` datetime DEFAULT NULL COMMENT '최근 비밀번호 수정일자',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

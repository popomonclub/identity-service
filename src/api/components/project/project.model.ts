import {
  Table,
  Column,
  Model,
  PrimaryKey,
  CreatedAt,
  UpdatedAt,
  BeforeCreate,
  BeforeUpdate
} from "sequelize-typescript";

@Table({ tableName: "project" }) // Default: TableName + 's' (Users)
class Project extends Model {
  @BeforeUpdate
  static updateLog(instance: Project) {
    console.log("Update project", instance.name);
  }

  @BeforeCreate
  static createLog(instance: Project) {
    console.log("Create project", instance.name);
  }

  @PrimaryKey
  @Column
  readonly id!: number;

  @Column
  name?: string;

  @Column
  host?: string;

  @Column
  test_host?: string;

  @Column
  reset_password_email_template?: string;

  @Column
  confirm_email_template?: string;

  @Column
  api_key?: string;

  @CreatedAt
  createdAt?: Date;

  @UpdatedAt
  updatedAt?: Date;

  @Column
  manager_id?: number;
}

export default Project;

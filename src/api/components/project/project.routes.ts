import express from "express";
import verifyAccessToken from "../../middleware/verifyAccessToken";
import projectController from "./project.controller";

const router = express.Router();

/**
 * 1. 내 프로젝트 등록/수정
 * 2. 내 프로젝트 이미지 등록/수정 + 이미지 에디터
 * 3. 내 프로젝트 URL/DEV_URL 등록/수정
 * 4. 등록 완료
 * 5. API KEY 발급
 */

router.get("/project", verifyAccessToken , projectController.getProjectListRequest);
router.get("/project/:id", verifyAccessToken, projectController.getProjectRequest);
router.post("/project", verifyAccessToken, projectController.addProjectRequest);
router.put("/project/:id", verifyAccessToken, projectController.updateProjectRequest);
router.delete("/project/:id", verifyAccessToken, projectController.removeProjectRequest);
router.get("/project/:id/apiKey", verifyAccessToken, projectController.getProjectAPIKeyRequest);

export default router;

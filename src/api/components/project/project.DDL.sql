CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '기본 키',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '프로젝트 이름',
  `host` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '프로젝트 호스트',
  `test_host` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '프로젝트 개발용 호스트',
  `reset_password_email_template` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '비밀번호 초기화 이메일 템플릿',
  `confirm_email_template` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '본인인증 이메일 템플릿',
  `api_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT 'API 키',
  `createdAt` datetime DEFAULT NULL COMMENT '생성일',
  `updatedAt` datetime DEFAULT NULL COMMENT '수정일',
  `manager_id` int DEFAULT NULL COMMENT '포린 키 (관리자)',
  PRIMARY KEY (`id`),
  KEY `manager_id` (`manager_id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

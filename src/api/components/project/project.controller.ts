import { NextFunction, Request, Response } from "express";
import {
  ValidationChain,
  validationResult
} from "express-validator";
import * as projectValidation from "./project.validation";
import * as projectService from "./project.service";
import generateToken from "../../helpers/generateToken";
import Project from "./project.model";
import sendEmail from "../../helpers/sendEmail";
import { ACCESS_TOKEN_NAME } from "../../env";
import matched from "../../helpers/matched";

const getProjectListValidation: ValidationChain[] = [];
async function getProjectList(req: Request, res: Response){}

const getProjectValidation: ValidationChain[] = [];
async function getProject(req: Request, res: Response){}

const addProjectValidation: ValidationChain[] = [];
async function addProject(req: Request, res: Response){}

const updateProjectValidation: ValidationChain[] = [];
async function updateProject(req: Request, res: Response){}

const removeProjectValidation: ValidationChain[] = [];
async function removeProject(req: Request, res: Response){}

const getProjectAPIKeyValidation: ValidationChain[] = [];
async function getProjectAPIKey(req: Request, res: Response){}

export default {
  getProjectListRequest: [...getProjectListValidation, getProjectList],
  getProjectRequest: [...getProjectValidation, getProject],
  addProjectRequest: [...addProjectValidation, addProject],
  updateProjectRequest: [...updateProjectValidation, updateProject],
  removeProjectRequest: [...removeProjectValidation, removeProject],
  getProjectAPIKeyRequest: [...getProjectAPIKeyValidation, getProjectAPIKey]
};
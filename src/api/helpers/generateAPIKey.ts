import jwt from "jsonwebtoken";
import { ACCESS_TOKEN_SECRET } from "../env";

function generateAPIKey(username) {
  return jwt.sign({ username }, ACCESS_TOKEN_SECRET || "", {});
}

export default generateAPIKey;
